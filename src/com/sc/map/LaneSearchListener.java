package com.sc.map;

import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.core.SearchResult;
import com.sc.app.LocationMarkType;
import com.sc.app.SPStorage;
import com.sc.db.DBStorage;
import com.sc.db.MarkPoint;
import com.sc.db.MarkRecord;

public class LaneSearchListener implements OnGetGeoCoderResultListener {
	private Context mContext;
	private LatLng pos;
	private int markType;

	public LaneSearchListener(Context mContext, LatLng pos, int markAsBegin) {
		this.mContext = mContext;
		this.pos = pos;
		this.markType = markAsBegin;
	}

	@Override
	public void onGetGeoCodeResult(GeoCodeResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetReverseGeoCodeResult(ReverseGeoCodeResult arg0) {
		switch(this.markType){
		case LocationMarkType.MARK_AS_BEGIN:
			beginMarkHandler(arg0);
			break;
		case LocationMarkType.MARK_AS_MIDDLE:
			middleMarkHandler(arg0);
			break;
		case LocationMarkType.MARK_AS_END:
			endMarkHandler(arg0);
			break;
		}
	}

	private void endMarkHandler(ReverseGeoCodeResult arg0) {
		int recordId = SPStorage.getCurrentRecordId(mContext);
		MarkPoint point = new MarkPoint();
		point.setLatitude(pos.latitude);
		point.setLongitude(pos.longitude);
		point.setMarkRecordId(recordId);
		point.setTime(new Date().toLocaleString());
		DBStorage.createMarkPoint(mContext, point);
		
		/* 标记结束 */
		MarkRecord record = DBStorage.getMarkRecordById(mContext, recordId);
		if(record != null){
			if(arg0.error == SearchResult.ERRORNO.NO_ERROR){
				record.setDestination(arg0.getAddress());
			}
			else{
				record.setDestination(pos.toString());
			}
			record.setStatus(MarkRecord.MARK_DONE);
			DBStorage.updateMarkRecord(mContext, record);
		}
		
		SPStorage.setCurrentRecordId(mContext, 0);
	}

	private void middleMarkHandler(ReverseGeoCodeResult arg0) {
		int recordId = SPStorage.getCurrentRecordId(mContext);
		MarkPoint point = new MarkPoint();
		point.setLatitude(pos.latitude);
		point.setLongitude(pos.longitude);
		point.setMarkRecordId(recordId);
		point.setTime(new Date().toLocaleString());
		DBStorage.createMarkPoint(mContext, point);
		SPStorage.setCurrentRecordId(mContext, recordId);
	}

	private void beginMarkHandler(ReverseGeoCodeResult arg0){
		MarkRecord record = new MarkRecord();
		if(arg0.error == SearchResult.ERRORNO.NO_ERROR){
			String address = arg0.getAddress();
			Log.i("info", "解析经纬度得到地址：" + address);
			
			record.setStarting(address);
		}
		else{
			record.setStarting(pos.toString());
		}
		
		record.setStatus(MarkRecord.MARK_INPROGRESS);
		DBStorage.createMarkRecord(mContext, record);
		
		/* 创建一个该记录对应的记录点数据 */
		int recordId = DBStorage.getLatestMarkRecordId(mContext);
		Log.i("info", "新创建的记录ID为：" + recordId);
		
		MarkPoint point = new MarkPoint();
		point.setLatitude(pos.latitude);
		point.setLongitude(pos.longitude);
		point.setMarkRecordId(recordId);
		point.setTime(new Date().toLocaleString());
		DBStorage.createMarkPoint(mContext, point);
		SPStorage.setCurrentRecordId(mContext, recordId);
	}
	
}