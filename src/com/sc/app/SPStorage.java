package com.sc.app;

import android.content.Context;
import android.content.SharedPreferences;

public class SPStorage {
	private static final String SP_MARK_TYPE = "MARK_TYPE";
	private static final String SP_RECORD_ID = "MARK_ID";
	
	public static void setCurrentMarkType(Context ctx, int type){
		SharedPreferences sp = ctx.getSharedPreferences("SP", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(SP_MARK_TYPE, type);
		editor.commit();
	}
	
	public static int getCurrentMarkType(Context ctx){
		SharedPreferences sp = ctx.getSharedPreferences("SP", 0);
		return sp.getInt(SP_MARK_TYPE, LocationMarkType.MARK_UNDEFINE);
	}
	
	public static void setCurrentRecordId(Context ctx, int id){
		SharedPreferences sp = ctx.getSharedPreferences("SP", 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(SP_RECORD_ID, id);
		editor.commit();
	}
	
	public static int getCurrentRecordId(Context ctx){
		SharedPreferences sp = ctx.getSharedPreferences("SP", 0);
		return sp.getInt(SP_RECORD_ID, 0);
	}
}
